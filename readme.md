# Chess App
### 1.0 development setup
* ``` virtualenv --python=python3.7 venv```
* ``` source venv/bin/activate```
* ``` pip install django djangorestframework psycopg2-binary```
* ``` django-admin startproject backend ```

### 2.0 server environment setup
* ``` sudo apt-get install python3.7 ```

### 2.1 server file system setup
* ``` sudo mkdir /usr/local/chessapp ```
* ``` sudo chown ubuntu.ubuntu /usr/local/chessapp ```
* ``` cd /usr/local/chessapp ```
* ``` virtualenv --python=python3.7 venv ```

### 2.2 command for setting up database
```
sudo su - postgres
psql

postgres=# CREATE database chessapp_db;
CREATE DATABASE
postgres=# CREATE user chessapp_dbuser with password '!@#cheSsbE';
CREATE ROLE
postgres=# ALTER role chessapp_dbuser set client_encoding to 'utf8';
ALTER ROLE
postgres=# ALTER role chessapp_dbuser set default_transaction_isolation to 'read committed';
ALTER ROLE
postgres=# ALTER role chessapp_dbuser set timezone to 'UTC';
ALTER ROLE
postgres=# GRANT all privileges on database chessapp_db to chessapp_dbuser;
GRANT
postgres=# \q
```
