import React, { Suspense, lazy } from "react";
import { addLocaleData, IntlProvider } from "react-intl";

import myEn from "./base/en";
import en from "react-intl/locale-data/en";
import zh from "react-intl/locale-data/zh";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

const ChessContainer = lazy(() => import("./chess/ChessContainer"));

addLocaleData([...en, ...zh]);

function App() {
  return (
    <IntlProvider locale="en" messages={myEn}>
      <Router>
        <Suspense fallback={<div>Loading...</div>}>
          <Switch>
            <Route path="/chess/" component={ChessContainer} />
          </Switch>
        </Suspense>
      </Router>
    </IntlProvider>
  );
}

export default App;
