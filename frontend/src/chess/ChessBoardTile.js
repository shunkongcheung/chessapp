import React, { memo,  useMemo } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

import IconButton from "@material-ui/core/IconButton";
import InfoIcon from "@material-ui/icons/Info";
import GridListTile from "@material-ui/core/GridListTile";
import GridListTileBar from "@material-ui/core/GridListTileBar";

import ChessBoard from "./ChessBoard";

function ChessBoardTile({
  id,
  board,
  upper_player_depth,
  upper_player_score,
  isLinkInitial = true,
  ...rest
}) {
  const renderedChessBoardAsThumbnail = useMemo(
    () => (
      <div style={{ display: "flex", justifyContent: "center" }}>
        <div style={{ width: 150, position: "relative" }}>
          <ChessBoard board={board} />
        </div>
      </div>
    ),
    [board]
  );

  const renderedChessBoardTileBar = useMemo(
    () => (
      <GridListTileBar
        title={`depth: ${upper_player_depth}`}
        subtitle={<span>{`score: ${upper_player_score}`}</span>}
        actionIcon={
          <Link
            to={
              id
                ? {
                    initialContent: isLinkInitial
                      ? {
                          id,
                          board,
                          upper_player_depth,
                          upper_player_score
                        }
                      : null,
                    pathname: `/chess/${id}/`
                  }
                : "#"
            }
          >
            <IconButton disabled={!id}>
              <InfoIcon />
            </IconButton>
          </Link>
        }
      />
    ),
    [board, id, isLinkInitial, upper_player_depth, upper_player_score]
  );

  return (
    <GridListTile {...rest}>
      {renderedChessBoardAsThumbnail}
      {renderedChessBoardTileBar}
    </GridListTile>
  );
}

ChessBoardTile.propTypes = {
  board: PropTypes.array.isRequired,
  id: PropTypes.number.isRequired,
  isLinkInitial: PropTypes.bool,
  upper_player_depth: PropTypes.number.isRequired,
  upper_player_score: PropTypes.number.isRequired
};
export default memo(ChessBoardTile);
