import React, { memo, useCallback, useMemo, useState } from "react";
import PropTypes from "prop-types";

import ChessPiece from "./ChessPiece";
import chessBoardImg from "./img/board.png";

function ChessBoard({ board, handleUserMove, validMoves }) {
  const defaultCoord = { row: -1, col: -1 };
  const [pieceCoord, setPieceCoord] = useState(defaultCoord);

  const getIsMatchCoord = useCallback(
    (coordA, coordB) => coordA.row === coordB.row && coordA.col === coordB.col,
    []
  );

  const getIsChessDisabled = useCallback(
    coord => {
      let validCoords = [];
      if (getIsMatchCoord(pieceCoord, defaultCoord)) {
        validCoords = validMoves.map(item => item[0]);
      } else {
        validCoords = validMoves.map(item => item[1]);
      }

      const isValidTarget = validCoords.reduce(
        (acc, item) => acc || getIsMatchCoord(item, coord),
        false
      );
      const isSelectedPiece = getIsMatchCoord(pieceCoord, coord);

      return !isValidTarget && !isSelectedPiece;
    },
    [defaultCoord, getIsMatchCoord, pieceCoord, validMoves]
  );

  const handlePieceClick = useCallback(
    clickCoord => {
      // clicking on the same piece
      if (getIsMatchCoord(clickCoord, pieceCoord))
        return setPieceCoord(defaultCoord);

      // none were selected
      if (getIsMatchCoord(pieceCoord, defaultCoord))
        return setPieceCoord(clickCoord);

      handleUserMove({ from: pieceCoord, to: clickCoord });
      setPieceCoord(defaultCoord);
    },
    [defaultCoord, getIsMatchCoord, handleUserMove, pieceCoord]
  );

  const renderedBoardBackground = useMemo(
    () => (
      <img
        alt="board"
        src={chessBoardImg}
        style={{ width: "100%", heigth: "auto" }}
      />
    ),
    []
  );

  const renderBoardPiece = useCallback((pieceCode, row, col) => {
    const widthPercent = 100.0 / board[0].length;
    const curCoord = { row, col };
    const disabled = getIsChessDisabled(curCoord);
    const isSelected = getIsMatchCoord(pieceCoord, curCoord);

    return (
      <div
        key={`boardPiece${col}`}
        style={{
          display: "flex",
          width: `${widthPercent}%`,
          height: "100%"
        }}
      >
        <ChessPiece
          disabled={disabled}
          handlePieceClick={() => handlePieceClick(curCoord)}
          isSelected={isSelected}
          pieceCode={pieceCode}
        />
      </div>
    );
  }, [board, getIsChessDisabled, getIsMatchCoord, handlePieceClick, pieceCoord]);

  const renderBoardRow = useCallback(
    (row, rowIdx) => {
      const rowHeightPercent = 100.0 / board.length;
      return (
        <div
          key={`boardRow${rowIdx}`}
          style={{
            display: "flex",
            width: "100%",
            height: `${rowHeightPercent}%`
          }}
        >
          {row.map((pieceCode, colIdx) =>
            renderBoardPiece(pieceCode, rowIdx, colIdx)
          )}
        </div>
      );
    },
    [board.length, renderBoardPiece]
  );

  const renderedBoard = useMemo(() => board.map(renderBoardRow), [
    board,
    renderBoardRow
  ]);

  return (
    <div style={{ position: "relative" }}>
      {renderedBoardBackground}
      <div
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          right: 0,
          bottom: 0
        }}
      >
        {renderedBoard}
      </div>
    </div>
  );
}

ChessBoard.propTypes = {
  board: PropTypes.array.isRequired,
  handleUserMove: PropTypes.func,
  validMoves: PropTypes.array
};
ChessBoard.defaultProps = {
  handleUserMove: () => {},
  validMoves: []
};

export default memo(ChessBoard);
