import React, { memo, useCallback, useMemo } from "react";
import { FormattedMessage } from "react-intl";
import PropTypes from "prop-types";

import Card from "@material-ui/core/Card";
import Grid from "@material-ui/core/Grid";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import LayersIcon from "@material-ui/icons/Layers";
import OpacityIcon from "@material-ui/icons/Opacity";
import ChessBoard from "./ChessBoard";

function ChessDetailBoardCard({
  board,
  upper_player_score,
  upper_player_depth
}) {
  const renderUpperPlayerVal = useCallback(
    (intlId, val) => (
      <Grid item xs={6}>
        <Grid container spacing={8}>
          <Grid item>
            {intlId.endsWith("depth") ? <LayersIcon /> : <OpacityIcon />}
          </Grid>
          <Grid item>
            <FormattedMessage
              id={`ChessDetailBoardCard.${intlId}`}
              values={val}
            />
          </Grid>
        </Grid>
      </Grid>
    ),
    []
  );

  const renderedDepth = useMemo(
    () => renderUpperPlayerVal("upper_player_depth", { upper_player_depth }),
    [renderUpperPlayerVal, upper_player_depth]
  );
  const renderedScore = useMemo(
    () => renderUpperPlayerVal("upper_player_score", { upper_player_score }),
    [renderUpperPlayerVal, upper_player_score]
  );
  return (
    <Card>
      <Typography component="div" style={{ width: 350 }}>
        <ChessBoard board={board} />
      </Typography>
      <CardContent>
        <Grid container spacing={24}>
          {renderedDepth}
          {renderedScore}
        </Grid>
      </CardContent>
    </Card>
  );
}
ChessDetailBoardCard.propTypes = {
  board: PropTypes.array.isRequired,
  upper_player_depth: PropTypes.number.isRequired,
  upper_player_score: PropTypes.number.isRequired
};
export default memo(ChessDetailBoardCard);
