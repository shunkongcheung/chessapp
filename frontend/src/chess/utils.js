export const pPrefix = {
  empty: "_",
  general: "G"
};

export const getFlippedBoard = board => {
  let flippedBoard = [];
  for (let idx = board.length - 1; idx >= 0; idx--) {
    flippedBoard.push(JSON.parse(JSON.stringify(board[idx])));
  }
  for (let row = 0; row < flippedBoard.length; row++) {
    for (let col = 0; col < flippedBoard[row].length; col++) {
      const curPVal = flippedBoard[row][col];
      if (curPVal === pPrefix.empty) continue;
      if (curPVal.toUpperCase() === curPVal)
        flippedBoard[row][col] = curPVal.toLowerCase();
      if (curPVal.toLowerCase() === curPVal)
        flippedBoard[row][col] = curPVal.toUpperCase();
    }
  }
  return flippedBoard;
};

export const getMovedBoard = (fromBoard, move) => {
  const { from, to } = move;
  const movedBoard = JSON.parse(JSON.stringify(fromBoard));
  movedBoard[to.row][to.col] = movedBoard[from.row][from.col];
  movedBoard[from.row][from.col] = pPrefix.empty;
  return movedBoard;
};

const getIsGeneralExist = (board, generalCode) =>
  board.reduce((acc, row) => {
    if (acc) return acc;
    return row.reduce(
      (acc, pieceCode) => acc || pieceCode === generalCode,
      false
    );
  }, false);

export const getBoardWinner = board => {
  const upGenCode = pPrefix.general.toUpperCase();
  const lowGenCode = pPrefix.general.toLowerCase();
  const isUpperGeneralExist = getIsGeneralExist(board, upGenCode);
  if (!isUpperGeneralExist) return lowGenCode;
  const isLowerGeneralExist = getIsGeneralExist(board, lowGenCode);
  if (!isLowerGeneralExist) return upGenCode;

  return pPrefix.empty;
};
