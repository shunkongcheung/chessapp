import { useCallback, useEffect, useMemo, useRef, useState } from "react";

import { CHS_PLAY_PREFIX } from "../base/actionPrefixes";
import { fetchPost } from "../base/makeFetch";
import { getMovedBoard, pPrefix, getBoardWinner } from "./utils";

export default function usePlayBoard({
  initialBoard = [],
  isFirstMoveByComp = false
}) {
  const isFirstFetched = useRef(false);
  const [meta, setMeta] = useState({ isLoading: false, errors: null });
  const [playRecords, setPlayRecords] = useState([
    { board: [], winner: pPrefix.empty, side: pPrefix.empty }
  ]);

  const fetchCompPlay = useCallback(async board => {
    const res = await fetchPost(CHS_PLAY_PREFIX, {
      data: { board, difficulity: 1, is_calculate_upper_side: true },
      suffix: "play"
    });
    const { ok, payload } = res;
    return { ok, payload };
  }, []);

  const lastPlayRecordsBoard = useMemo(
    () => {
      const { board } = playRecords[playRecords.length - 1];
      return board;
    },
    [playRecords]
  );

  const pushPlayRecord = useCallback((board, winner, side) => {
    setPlayRecords(state => [...state, { board, winner, side }]);
  }, []);

  const pushCompPlay = useCallback(
    async oldBoard => {
      setMeta({ isLoading: false, errors: false });
      const { ok, payload } = await fetchCompPlay(oldBoard);
      if (!ok) return setMeta({ isLoading: false, errors: payload });

      const { board, winner } = payload;
      const upperSide = pPrefix.general.toUpperCase();
      pushPlayRecord(board, winner, upperSide);
      setMeta({ isLoading: false, errors: false });
    },
    [fetchCompPlay, pushPlayRecord]
  );

  const popPlayRecords = useCallback(isPopTwice => {
    setPlayRecords(state => {
      const nState = [...state];
      nState.pop();
      if (isPopTwice) nState.pop();
      return nState;
    });
  }, []);

  const handleUserMove = useCallback(
    async move => {
      const curBoard = lastPlayRecordsBoard;
      const movedBoard = getMovedBoard(curBoard, move);

      const winner = getBoardWinner(movedBoard);
      pushPlayRecord(movedBoard, winner, pPrefix.general.toLowerCase());
      if (winner !== pPrefix.empty) return;

      pushCompPlay(movedBoard);
    },
    [lastPlayRecordsBoard, pushCompPlay, pushPlayRecord]
  );

  useEffect(
    () => {
      if (initialBoard.length) {
        pushPlayRecord(initialBoard, pPrefix.empty, pPrefix.empty);
      }
    },
    [initialBoard, initialBoard.length, pushPlayRecord]
  );

  useEffect(
    () => {
      if (isFirstFetched.current) return;
      if (!isFirstMoveByComp) return;

      isFirstFetched.current = true;
      const board = lastPlayRecordsBoard;
      pushCompPlay(board);
    },
    [lastPlayRecordsBoard, isFirstMoveByComp, pushCompPlay]
  );

  return {
    playBoard: lastPlayRecordsBoard,
    handleUserMove,
    popPlayRecords,
    meta
  };
}
