import React, { memo, useMemo } from "react";
import PropTypes from "prop-types";

import Fab from "@material-ui/core/Fab";
import ArrowLeftIcon from "@material-ui/icons/ArrowLeft";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";

function ChessDirBtn({ disabled = false, direction, handleBtnClick }) {
  const arrowIcon = useMemo(
    () => {
      if (direction === "left") return <ArrowLeftIcon />;
      if (direction === "right") return <ArrowRightIcon />;
    },
    [direction]
  );

  return (
    <Fab
      color="primary"
      disabled={disabled}
      size="small"
      onClick={handleBtnClick}
    >
      {arrowIcon}
    </Fab>
  );
}

ChessDirBtn.propTypes = {
  disabled: PropTypes.bool,
  direction: PropTypes.oneOf(["left", "right"]).isRequired,
  handleBtnClick: PropTypes.func.isRequired
};

export default memo(ChessDirBtn);
