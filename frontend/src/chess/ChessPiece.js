import React, { memo, useCallback, useMemo } from "react";
import PropTypes from "prop-types";
import ButtonBase from "@material-ui/core/ButtonBase";

import chessPiecesImg from "./img/pieces.png";
import transImg from "./img/trans.gif";

import "./ChessPiece.css";

function ChessPiece({ disabled, handlePieceClick, isSelected, pieceCode }) {
  const animationCss = useMemo(
    () => {
      if (!isSelected) return null;
      return "jump 1.5s ease 0s infinite normal";
    },
    [isSelected]
  );

  const getPieceOffset = useCallback(pieceCode => {
    const codeOrder = ["g", "k", "j", "c", "h", "a", "s"];
    return codeOrder.findIndex(item => item === pieceCode);
  }, []);

  const _handlePieceClick = useCallback(() => handlePieceClick(pieceCode), [
    handlePieceClick,
    pieceCode
  ]);

  const imgBackground = useMemo(
    () => {
      let [hOffset, vOffset] = [0, 0];
      const pLower = pieceCode.toLowerCase();
      if (pieceCode === pLower) vOffset = 100;
      hOffset = getPieceOffset(pLower);

      const bgPosition = `${(100.0 / 7 + 2.4) * hOffset}% ${vOffset}%`;
      const bgSize = "700% 200%";
      const background =
        hOffset >= 0 ? `url(${chessPiecesImg}) ${bgPosition} / ${bgSize}` : "";

      return background;
    },
    [getPieceOffset, pieceCode]
  );

  return (
    <ButtonBase
      disabled={disabled}
      style={{
        width: "100%",
        height: "100%",
        WebkitAnimation: animationCss,
        animation: animationCss
      }}
      onClick={_handlePieceClick}
    >
      <img
        alt="board"
        src={transImg}
        style={{
          background: imgBackground,
          width: "100%",
          height: "100%"
        }}
      />
    </ButtonBase>
  );
}

ChessPiece.defaultProps = {
  disabled: false,
  isSelected: false,
  handlePieceClick: () => {}
};

ChessPiece.propTypes = {
  disabled: PropTypes.bool,
  handlePieceClicked: PropTypes.func,
  isSelected: PropTypes.bool,
  pieceCode: PropTypes.string.isRequired
};

export default memo(ChessPiece);
