import React, { memo, useCallback, useEffect, useMemo, useState } from "react";

import Fade from "@material-ui/core/Fade";
import Typography from "@material-ui/core/Typography";

import { CHS_PLAY_PREFIX } from "../base/actionPrefixes";
import { fetchGet } from "../base/makeFetch";
import Layout from "../base/Layout";

import ChessSidePicker from "./ChessSidePicker";
import ChessPlayBoard from "./ChessPlayBoard";
import { pPrefix } from "./utils";
import ErrorContent from "../base/ErrorContent";

const ChessPlayGroundChooseFirstMoveSide = memo(({ handleSidePicked }) => {
  return (
    <Typography
      component="div"
      style={{
        alignItems: "center",
        display: "flex",
        justifyContent: "center",
        height: "100%"
      }}
    >
      <div
        style={{
          fontWeight: 600,
          width: 200
        }}
      >
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            marginBottom: 10
          }}
        >
          Choose First Move User
        </div>
        <div style={{ width: "100%" }}>
          <ChessSidePicker handleSidePicked={handleSidePicked} />
        </div>
      </div>
    </Typography>
  );
});

function ChessPlayGround() {
  const [meta, setMeta] = useState({ isLoading: false, errors: null });
  const [firstMoveSide, setFirstMoveSide] = useState(pPrefix.empty);
  const [initialBoard, setInitBoard] = useState([]);

  const fetchInitBoard = useCallback(async () => {
    const res = await fetchGet(CHS_PLAY_PREFIX, { suffix: "init_board" });
    const { ok, payload } = res;
    return { ok, payload };
  }, []);

  const initializeInitBoard = useCallback(
    async () => {
      setMeta({ isLoading: true, errors: null });
      const boardRes = await fetchInitBoard();
      if (!boardRes.ok)
        return setMeta({ isLoading: false, errors: boardRes.payload });

      const { board } = boardRes.payload;
      setInitBoard(board);
      setMeta({ isLoading: false, errors: null });
    },
    [fetchInitBoard]
  );

  useEffect(
    () => {
      initializeInitBoard();
    },
    [initializeInitBoard]
  );

  const stageChildren = useMemo(
    () => {
      const isMs = firstMoveSide === pPrefix.empty;
      return (
        <>
          <div style={{ display: isMs ? "block" : "none" }}>
            <Fade in={isMs} timeout={1000}>
              <ChessPlayGroundChooseFirstMoveSide
                handleSidePicked={setFirstMoveSide}
              />
            </Fade>
          </div>
          <Fade in={!isMs} timeout={1000}>
            <div style={{ display: !isMs ? "block" : "none" }}>
              <ChessPlayBoard
                firstMoveSide={firstMoveSide}
                initialBoard={initialBoard}
              />
            </div>
          </Fade>
        </>
      );
    },
    [firstMoveSide, initialBoard]
  );

  return (
    <Layout>
      {meta.errors ? <ErrorContent details={meta.errors} /> : stageChildren}
    </Layout>
  );
}

export default memo(ChessPlayGround);
