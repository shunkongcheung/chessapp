import React, { memo, useCallback, useEffect, useState } from "react";
import PropTypes from "prop-types";

import Grid from "@material-ui/core/Grid";
import GridList from "@material-ui/core/GridList";

import { fetchGetList } from "../base/makeFetch";
import { CHS_MASTER_PREFIX } from "../base/actionPrefixes";
import ChessBoard from "./ChessBoard";
import useValidMoves from "./useValidMoves";
import usePlayBoard from "./usePlayBoard";
import useChessDetailState from "./useChessDetailState";
import { pPrefix, getFlippedBoard } from "./utils";
import ChessBoardTile from "./ChessBoardTile";

function ChessPlayBoard({ firstMoveSide, initialBoard }) {
  const [validMoves, setValidMoves] = useValidMoves({});
  const { playBoard, handleUserMove } = usePlayBoard({
    initialBoard,
    isFirstMoveByComp: firstMoveSide === pPrefix.general.toUpperCase()
  });
  const [boardId, setBoardId] = useState(-1);

  const getBoardId = useCallback(async board => {
    const flippedBoard = getFlippedBoard(board);
    const playBoardHash = flippedBoard.map(row => row.join("")).join("");
    const { results, ok } = await fetchGetList(CHS_MASTER_PREFIX, {
      queryParams: {
        board_hash: playBoardHash
      }
    });
    if (ok && results.length) return results[0].id;
    return -1;
  }, []);

  const [detailState] = useChessDetailState(boardId);

  const handlePlayBoardChange = useCallback(
    async board => {
      const id = await getBoardId(board);
      if (id > 0) setBoardId(id);
    },
    [getBoardId]
  );

  useEffect(
    () => {
      setValidMoves(playBoard);
      handlePlayBoardChange(playBoard);
    },
    [handlePlayBoardChange, playBoard, setValidMoves]
  );

  return (
    <Grid container>
      <Grid
        item
        xs={8}
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <div style={{ width: "80%" }}>
          <ChessBoard
            board={playBoard}
            handleUserMove={handleUserMove}
            validMoves={validMoves}
          />
        </div>
      </Grid>
      <Grid item xs={4} style={{ height: "90vh" }}>
        <GridList cellHeight={180} cols={1} style={{ height: "100%" }}>
          {detailState.movedBoards.map((item, idx) => (
            <ChessBoardTile
              key={item.board_hash}
              {...item}
              isLinkInitial={false}
            />
          ))}
        </GridList>
      </Grid>
    </Grid>
  );
}

ChessPlayBoard.propTypes = {
  firstMoveSide: PropTypes.string.isRequired,
  initialBoard: PropTypes.array.isRequired
};

export default memo(ChessPlayBoard);
