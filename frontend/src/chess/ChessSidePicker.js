import React, { memo, useCallback, useEffect, useState } from "react";
import Fade from "@material-ui/core/Fade";
import PropTypes from "prop-types";

import ChessDirBtn from "./ChessDirBtn";
import ChessPiece from "./ChessPiece";

function ChessSidePicker({ handleSidePicked }) {
  const [pieceSide, setPieceSide] = useState("g");
  const [isIn, setIsIn] = useState(false);

  const handleDirectionClick = useCallback(() => {
    setIsIn(false);
    setTimeout(() => {
      setPieceSide(pieceSide => (pieceSide === "g" ? "G" : "g"));
    }, 500);
  }, []);

  const handlePieceClick = useCallback(
    () => {
      handleSidePicked(pieceSide);
    },
    [handleSidePicked, pieceSide]
  );

  useEffect(
    () => {
      setIsIn(true);
    },
    [pieceSide]
  );

  return (
    <div
      style={{
        alignItems: "center",
        display: "flex",
        justifyContent: "space-around",
        width: "100%",
        height: 50
      }}
    >
      <ChessDirBtn direction="left" handleBtnClick={handleDirectionClick} />
      <Fade in={isIn} timeout={1000}>
        <div
          style={{
            position: "relative",
            width: "25%",
            height: "100%"
          }}
        >
          <ChessPiece
            handlePieceClick={handlePieceClick}
            pieceCode={pieceSide}
          />
        </div>
      </Fade>
      <ChessDirBtn direction="right" handleBtnClick={handleDirectionClick} />
    </div>
  );
}

ChessSidePicker.propTypes = {
  handleSidePicked: PropTypes.func.isRequired
};

export default memo(ChessSidePicker);
