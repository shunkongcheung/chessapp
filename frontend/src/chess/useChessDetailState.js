import { useCallback, useEffect, useState } from "react";

import { CHS_MASTER_PREFIX, CHS_PLAY_PREFIX } from "../base/actionPrefixes";
import { fetchGet, fetchPost, fetchGetList } from "../base/makeFetch";
import { getFlippedBoard, getMovedBoard } from "./utils";

export default function useChessDetailState(id, initialContent) {
  const [meta, setMeta] = useState({
    errors: null,
    isCtLoading: true,
    isMbLoading: true
  });
  const [content, setContent] = useState(
    initialContent || {
      board: [],
      upper_player_depth: 0,
      upper_player_score: 0
    }
  );
  const [movedBoards, setMovedBoards] = useState([]);

  const getBoardHash = useCallback(
    board => board.map(row => row.join("")).join(""),
    []
  );

  const getMovedBoardsWithoutScore = useCallback((oriBoard, validMoves) => {
    return validMoves.map(move => {
      return getFlippedBoard(
        getMovedBoard(oriBoard, { from: move[0], to: move[1] })
      );
    });
  }, []);

  const getMovedBoardsWithScore = useCallback(
    async movedBoardsWithoutScore => {
      const boardHashes = movedBoardsWithoutScore.map(item =>
        getBoardHash(item)
      );
      const queryParams = { board_hash__in: boardHashes.join(",") };
      const { errors, ok, results } = await fetchGetList(CHS_MASTER_PREFIX, {
        queryParams
      });
      if (!ok) return { ok, errors };

      const movedBoardsWithScore = movedBoardsWithoutScore.map((item, idx) => {
        const boardInResults = results.find(
          rItem => rItem.board_hash === boardHashes[idx]
        );
        return boardInResults ? boardInResults : { board: item };
      });

      movedBoardsWithScore.sort(
        (a, b) => a.upper_player_score - b.upper_player_score
      );

      movedBoardsWithScore.map(
        item => (item.board = getFlippedBoard(item.board))
      );
      return { ok, payload: movedBoardsWithScore };
    },
    [getBoardHash]
  );

  const fetchValidMoves = useCallback(async board => {
    const vmKwargs = {
      suffix: "get_valid_moves",
      data: { from_board: board, is_upper_side: true }
    };
    const vRes = await fetchPost(CHS_PLAY_PREFIX, vmKwargs);
    const { errors, ok, payload } = vRes;
    if (!ok) return { ok, errors };
    return { ok, payload: payload.valid_moves };
  }, []);

  const fetchChessMovedBoards = useCallback(
    async content => {
      const { board } = content;
      // fetch valid moves
      const vRes = await fetchValidMoves(board);
      if (!vRes.ok) return vRes;

      //  moved boards
      const { payload: validMoves } = vRes;
      const movedBoardsWithoutScore = getMovedBoardsWithoutScore(
        board,
        validMoves
      );
      return await getMovedBoardsWithScore(movedBoardsWithoutScore);
    },
    [fetchValidMoves, getMovedBoardsWithScore, getMovedBoardsWithoutScore]
  );

  const fetchChessDetail = useCallback(
    async id => {
      if (initialContent) return { ok: true, payload: initialContent };
      return await fetchGet(CHS_MASTER_PREFIX, { suffix: id });
    },
    [initialContent]
  );

  const setChessDetailState = useCallback(
    async id => {
      setMeta({ isLoading: true });
      const {
        errors: cErrors,
        ok: cOk,
        payload: content
      } = await fetchChessDetail(id);
      if (!cOk) return setMeta({ isLoading: false, errors: cErrors });
      setContent(content);
      setMeta({ isCtLoading: false, isMbLoading: true, errors: null });

      const {
        errors: mErrors,
        ok: mOk,
        payload: movedBoards
      } = await fetchChessMovedBoards(content);
      if (!mOk) return setMeta({ isLoading: false, errors: mErrors });

      setMeta({ isCtLoading: false, isMbLoading: false, errors: null });
      setMovedBoards(movedBoards);
    },
    [fetchChessDetail, fetchChessMovedBoards]
  );

  useEffect(
    () => {
      setChessDetailState(id);
    },
    [id, setChessDetailState]
  );

  return [{ content, movedBoards }, meta];
}
