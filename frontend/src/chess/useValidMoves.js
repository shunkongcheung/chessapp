import { useCallback, useState } from "react";

import { CHS_PLAY_PREFIX } from "../base/actionPrefixes";
import { fetchPost } from "../base/makeFetch";

export default function useValidMoves({ is_upper_side = false }) {
  const [meta, setMeta] = useState({ isLoading: false, errors: null });
  const [validMoves, setValidMovesInternal] = useState([]);

  const fetchValidMoves = useCallback(
    async board => {
      const kwargs = {
        data: { from_board: board, is_upper_side },
        suffix: "get_valid_moves"
      };
      const res = await fetchPost(CHS_PLAY_PREFIX, kwargs);
      const { ok, payload } = res;
      if (!res.ok) return { ok, payload };

      const { valid_moves } = payload;
      return { ok, payload: valid_moves };
    },
    [is_upper_side]
  );

  const setValidMoves = useCallback(
    async board => {
      setMeta({ isLoading: true, errors: null });
      const { ok, payload } = await fetchValidMoves(board);
      if (!ok) return setMeta({ isLoading: false, errors: payload });

      setValidMovesInternal(payload);
      setMeta({ isLoading: false, errors: null });
    },
    [fetchValidMoves]
  );

  return [validMoves, setValidMoves, meta];
}
