import React, { memo } from "react";
import { Route, Switch } from "react-router-dom";

import ChessDetailView from "./ChessDetailView";
import ChessPlayView from "./ChessPlayView";
import ChessListView from "./ChessListView";

export default memo(function ChessView({ match }) {
  const detailUrl = `${match.url}/:id(\\d+)/`;
  const listUrl = `${match.url}/list/`;
  const playUrl = `${match.url}/play/`;
  return (
    <Switch>
      <Route path={detailUrl} exact component={ChessDetailView} />
      <Route path={listUrl} exact component={ChessListView} />
      <Route path={playUrl} exact component={ChessPlayView} />
    </Switch>
  );
});
