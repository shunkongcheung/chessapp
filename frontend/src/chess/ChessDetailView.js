import React, { memo, useMemo } from "react";

import CircularProgress from "@material-ui/core/CircularProgress";
import Grid from "@material-ui/core/Grid";
import GridList from "@material-ui/core/GridList/GridList";

import ErrorContent from "../base/ErrorContent";
import Layout from "../base/Layout";

import ChessBoardTile from "./ChessBoardTile";
import ChessDetailBoardCard from "./ChessDetailBoardCard";
import useChessDetailState from "./useChessDetailState";

export default memo(function ChessDetailView({ location, match }) {
  const { params } = match;
  const { initialContent } = location;
  const [res, meta] = useChessDetailState(params.id, initialContent);

  const { isMbLoading, errors } = meta;
  const { content, movedBoards = [] } = res;

  const renderedMovedBaords = useMemo(
    () => {
      return (
        <div style={{ marginRight: 20, paddingRight: 5, height: "100%" }}>
          <GridList cellHeight={180} cols={1} style={{ height: "100%" }}>
            {movedBoards.map((item, idx) => (
              <ChessBoardTile
                key={item.board_hash}
                {...item}
                isLinkInitial={false}
              />
            ))}
          </GridList>
        </div>
      );
    },
    [movedBoards]
  );

  const renderedMovedBaordsLoading = useMemo(() => {
    return (
      <div
        style={{
          width: "100%",
          height: "100%",
          display: "flex",
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <CircularProgress />
      </div>
    );
  }, []);

  return (
    <Layout>
      {errors ? (
        <ErrorContent details={errors} />
      ) : (
        <Grid container spacing={16} style={{ height: "100%" }}>
          <Grid item xs={12} sm={8}>
            <Grid
              container
              alignItems="center"
              justify="center"
              style={{ height: "100%" }}
            >
              <Grid item>
                <ChessDetailBoardCard {...content} />
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12} sm={4} style={{ height: "100%" }}>
            {isMbLoading ? renderedMovedBaordsLoading : renderedMovedBaords}
          </Grid>
        </Grid>
      )}
    </Layout>
  );
});
