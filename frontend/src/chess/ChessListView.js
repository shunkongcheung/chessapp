import React, {
  memo,
  useCallback,
  useEffect,
  useRef,
  useMemo,
  useState
} from "react";
import { injectIntl, FormattedMessage } from "react-intl";
import Select from "react-select";

import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import Fab from "@material-ui/core/Fab";
import Fade from "@material-ui/core/Fade";
import GridList from "@material-ui/core/GridList/GridList";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import SearchIcon from "@material-ui/icons/Search";

import { CHS_MASTER_PREFIX } from "../base/actionPrefixes";
import Layout from "../base/Layout";
import ErrorContent from "../base/ErrorContent";
import useListState from "../base/useListState";
import ChessBoardTile from "./ChessBoardTile";

function ChessListView({ intl }) {
  const [
    { results: chessBoardList },
    setListState,
    { errors, isLoading },
    { order_by }
  ] = useListState(CHS_MASTER_PREFIX, {
    isFetchOnMount: false,
    page_size: 10
  });
  const isMounted = useRef(false);

  const [isFilterFormOpen, setFilterFormOpen] = useState(false);

  useEffect(
    () => {
      if (!isMounted.current) {
        isMounted.current = true;
        setListState({ order_by: "-upper_player_depth" });
      }
    },
    [order_by, setListState]
  );

  const handleFetchButtonClick = useCallback(() => setListState({ order_by }), [
    order_by,
    setListState
  ]);

  const handleFilterBtnClick = useCallback(
    () => setFilterFormOpen(val => !val),
    []
  );

  const handleOrderByChange = useCallback(
    ({ value: order_by }) => {
      setFilterFormOpen(false);
      setListState({ order_by });
    },
    [setListState]
  );

  const renderedChessList = useMemo(
    () =>
      chessBoardList.map(item => {
        return <ChessBoardTile key={item.board_hash} {...item} />;
      }),
    [chessBoardList]
  );

  const renderedFetchButton = useMemo(
    () =>
      isLoading ? (
        <CircularProgress />
      ) : (
        <Button
          variant="outlined"
          color="primary"
          onClick={handleFetchButtonClick}
        >
          <FormattedMessage id="btn.more" />
        </Button>
      ),
    [handleFetchButtonClick, isLoading]
  );

  const renderedFilterBtn = useMemo(
    () => (
      <div style={{ width: "100%", textAlign: "end" }}>
        <Fab color="default" onClick={handleFilterBtnClick}>
          <SearchIcon />
        </Fab>
      </div>
    ),
    [handleFilterBtnClick]
  );

  const renderedFilterForm = useMemo(
    () => {
      const orderByChoices = ["-upper_player_depth", "-upper_player_score"].map(
        suggestion => ({
          value: suggestion,
          label: intl.formatMessage({ id: `ChessListView.${suggestion}` })
        })
      );
      return (
        <Grow
          in={isFilterFormOpen}
          style={{ transformOrigin: "-1 -1 0" }}
          timeout={500}
        >
          <div style={{ margin: 20 }}>
            <Paper>
              <Typography component="div" style={{ width: 200, padding: 30 }}>
                <Select
                  options={orderByChoices}
                  onChange={handleOrderByChange}
                  placeholder="Select order by"
                  value={{
                    value: order_by,
                    label: intl.formatMessage({
                      id: `ChessListView.${order_by}`
                    })
                  }}
                />
              </Typography>
            </Paper>
          </div>
        </Grow>
      );
    },
    [handleOrderByChange, intl, isFilterFormOpen, order_by]
  );

  const renderedFilter = useMemo(
    () => (
      <Fade in={!isLoading} timeout={500}>
        <div style={{ position: "absolute", bottom: 15, right: 20 }}>
          {renderedFilterForm}
          {renderedFilterBtn}
        </div>
      </Fade>
    ),
    [isLoading, renderedFilterBtn, renderedFilterForm]
  );

  return (
    <Layout>
      {errors ? (
        <ErrorContent details={errors} />
      ) : (
        <>
          <GridList cellHeight={180} spacing={20} cols={4}>
            {renderedChessList}
          </GridList>
          <Typography component="div" align="center" style={{ margin: 10 }}>
            {renderedFetchButton}
          </Typography>
          {renderedFilter}
        </>
      )}
    </Layout>
  );
}
export default memo(injectIntl(ChessListView));
