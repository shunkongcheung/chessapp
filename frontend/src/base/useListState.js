import { useCallback, useEffect, useRef, useState } from "react";
import { fetchGetList } from "./makeFetch";

export default function useListState(
  prefix,
  { isFetchOnMount = true, page_size }
) {
  const isInitialMount = useRef(true);
  const [meta, setMeta] = useState({
    errors: null,
    isLoading: false
  });
  const [params, setParams] = useState({
    order_by: "-modified_at",
    page: 1,
    page_size,
    queryParams: {}
  });
  const [res, setRes] = useState({
    results: [],
    maxPage: 0
  });

  const getIsSame = useCallback(
    (val1, val2) => JSON.stringify(val1) === JSON.stringify(val2),
    []
  );

  const getIsMerge = useCallback((oldResults, page) => {
    return oldResults.length > 0 && page !== 1;
  }, []);

  const getSetRes = useCallback(
    (res, page) => {
      const { maxPage, results: newResults } = res;
      return ({ results: _results }) => {
        const isMerge = getIsMerge(_results, page);
        const results = isMerge ? [..._results, ...newResults] : newResults;
        return { results, maxPage };
      };
    },
    [getIsMerge]
  );

  const handleParamsChange = useCallback(
    async () => {
      setMeta({ errors: null, isLoading: true });
      const res = await fetchGetList(prefix, params);
      const { errors, ok } = res;
      if (!ok) return setMeta({ errors, isLoading: false });
      setRes(getSetRes(res, params.page));
      setMeta({ errors: null, isLoading: false });
    },
    [getSetRes, params, prefix]
  );

  const setListState = useCallback(
    ({ order_by, queryParams }) => {
      setParams(state => {
        const {
          order_by: oOrder_by,
          queryParams: oQueryParams,
          page: oPage
        } = state;

        const isOrderBySame = getIsSame(order_by || oOrder_by, oOrder_by);
        const isQuParamsSame = getIsSame(
          queryParams || oQueryParams,
          oQueryParams
        );

        if (isOrderBySame && isQuParamsSame)
          return { ...state, page: oPage + 1 };
        else return { ...state, order_by, queryParams, page: 1 };
      });
    },
    [getIsSame]
  );

  useEffect(
    () => {
      if (isInitialMount.current) {
        isInitialMount.current = false;
        if (!isFetchOnMount) return;
      }
      handleParamsChange();
    },
    [params, handleParamsChange, isFetchOnMount]
  );

  return [res, setListState, meta, params];
}
