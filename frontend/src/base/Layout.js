import React, { memo, useCallback, useState } from "react";
import PropTypes from "prop-types";

import Fade from "@material-ui/core/Fade";
import TopNavigation from "./TopNavigation";
import LeftDrawer from "./LeftDrawer";

function Layout({ children, fadeSpeed = 1500, isIn = true }) {
  const [isLeftDrawerOpen, setIsLeftDrawerOpen] = useState(false);
  const handleMenuIconClick = useCallback(() => setIsLeftDrawerOpen(true), []);
  const handleDrawerClose = useCallback(() => setIsLeftDrawerOpen(false), []);

  return (
    <div>
      <TopNavigation handleMenuIconClick={handleMenuIconClick} />
      <LeftDrawer
        isOpen={isLeftDrawerOpen}
        handleDrawerClose={handleDrawerClose}
      />
      <Fade in={isIn} timeout={fadeSpeed}>
        <div
          style={{
            height: "90vh",
            padding: 20,
            overflowY: "auto"
          }}
        >
          {children}
        </div>
      </Fade>
    </div>
  );
}
PropTypes.propTypes = {
  children: PropTypes.element.isRequired,
  fadeSpeed: PropTypes.number,
  isIn: PropTypes.bool
};

export default memo(Layout);
