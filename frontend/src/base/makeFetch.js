const getAuthorization = isAuthenticated => {
  return isAuthenticated ? "" : ""; // TODO
};

const getBody = (data, method) => {
  if (method !== "POST" && method !== "PUT") return;
  data = data || {};
  return JSON.stringify(data);
};

const getExternalPrefix = isExternal => (isExternal ? "" : "/api");

const getQueryString = queryParams => {
  if (!Object.keys(queryParams).length) return "";
  let queryString = Object.entries(queryParams)
    .map(([key, val]) => `${key}=${val}`)
    .join("&");
  queryString = `?${queryString}`;
  return queryString;
};

const getJsonParsedPayload = async res => {
  let payload = "invalid payload";
  try {
    payload = await res.text();
  } catch (ex) {}
  try {
    payload = JSON.parse(payload);
  } catch (ex) {
    payload = { error: payload };
  }
  return payload;
};

const makeFetch = async (
  url,
  {
    data,
    isExternal = false,
    isAuthenticated = true,
    method = "GET",
    queryParams = {}
  }
) => {
  const queryString = getQueryString(queryParams);
  const externalPrefix = getExternalPrefix(isExternal);
  url = `${externalPrefix}/${url}${queryString}`;

  const Authorization = getAuthorization(isAuthenticated);
  const body = getBody(data, method);
  const headers = {
    "Content-Type": "application/json",
    Authorization
  };

  const res = await fetch(url, { headers, body, method });
  const payload = await getJsonParsedPayload(res);
  return { ok: res.ok, errors: payload, status: res.status, payload };
};

export const fetchGet = (prefix, { suffix = "list", queryParams = {} }) => {
  const url = `${prefix}/${suffix}/`;
  return makeFetch(url, { queryParams });
};

export const fetchGetList = async (
  prefix,
  { page = 1, page_size = 100, order_by = "-modified_at", queryParams = {} }
) => {
  const { errors, ok, status, payload } = await fetchGet(prefix, {
    queryParams: { ...queryParams, order_by, page, page_size }
  });
  if (!ok) return { errors, ok, status };

  const { count, results } = payload;
  const maxPage = Math.ceil((1.0 * count) / page_size);
  return { errors: null, ok, maxPage, results };
};

export const fetchPost = (prefix, { data = {}, suffix = "create" }) => {
  const url = `${prefix}/${suffix}/`;
  return makeFetch(url, { data, method: "POST" });
};
