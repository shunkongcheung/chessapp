import React, { memo, useMemo } from "react";
import { injectIntl } from "react-intl";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";

import GamesIcon from "@material-ui/icons/Games";
import ListIcon from "@material-ui/icons/List";

function LeftDrawer({ intl, isOpen, handleDrawerClose }) {
  const drawerList = useMemo(
    () => [
      { to: "/chess/play/", icon: <GamesIcon /> },
      { to: "/chess/list/", icon: <ListIcon /> },
      "divider"
    ],
    []
  );

  const renderedDrawerList = useMemo(
    () =>
      drawerList.map(
        (item, index) =>
          typeof item == "object" ? (
            <Link to={item.to} key={item.to}>
              <ListItem button key={item.to}>
                <ListItemIcon>{item.icon}</ListItemIcon>
                <ListItemText
                  primary={intl.formatMessage({
                    id: `LeftDrawer.${item.to}`
                  })}
                />
              </ListItem>
            </Link>
          ) : (
            <Divider light key={`LeftDrawer.divider.${index}`} />
          )
      ),

    [drawerList, intl]
  );

  return (
    <Drawer anchor="left" open={isOpen} onClose={handleDrawerClose}>
      <List style={{ width: 250 }}>{renderedDrawerList}</List>
    </Drawer>
  );
}

LeftDrawer.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleDrawerClose: PropTypes.func.isRequired
};
export default injectIntl(memo(LeftDrawer));
