import React, { memo, useCallback, useMemo } from "react";
import { FormattedMessage } from "react-intl";
import PropTypes from "prop-types";

import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Collapse from "@material-ui/core/Collapse";
import IconButton from "@material-ui/core/IconButton";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Typography from "@material-ui/core/Typography";

function ErrorContent({ details, statusCode, handleRetryClick }) {
  const [expanded, setExpanded] = React.useState(false);
  const handleExpandClick = useCallback(() => setExpanded(val => !val), []);

  const renderDetail = useCallback(detail => {
    if (typeof detail === "object") {
      return Object.values(detail).map(item => renderDetail(item));
    }
    return (
      <Typography components="p" paragraph>
        {detail}
      </Typography>
    );
  }, []);

  const renderedBanner = useMemo(
    () => (
      <CardContent>
        <Typography component="p" variant="h5" gutterBottom>
          <FormattedMessage id={`ErrorContent.title.${statusCode}`} />
        </Typography>
        <Typography component="p" variant="body1">
          <FormattedMessage id={`ErrorContent.content.${statusCode}`} />
        </Typography>
      </CardContent>
    ),
    [statusCode]
  );

  const renderedControl = useMemo(
    () => (
      <CardActions
        disableActionSpacing
        style={{ display: "flex", justifyContent: "end" }}
      >
        <IconButton
          aria-expanded={expanded}
          aria-label="Show more"
          disabled={!details}
          onClick={handleExpandClick}
          style={{ marginLeft: "auto" }}
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
    ),
    [details, expanded, handleExpandClick]
  );

  const renderedDetail = useMemo(
    () => (
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography component="p" variant="body2" gutterBottom>
            {renderDetail(details)}
          </Typography>
        </CardContent>
      </Collapse>
    ),
    [details, expanded, renderDetail]
  );

  return (
    <Card>
      {renderedBanner}
      {renderedControl}
      {renderedDetail}
    </Card>
  );
}

ErrorContent.propTypes = {
  details: PropTypes.any,
  statusCode: PropTypes.oneOf([400, 401, 403, 404, 500]),
  handleRetryClick: PropTypes.func.isRequired
};
ErrorContent.defaultProps = {
  statusCode: 500
};

export default memo(ErrorContent);
