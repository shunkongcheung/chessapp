import React, { memo } from "react";

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";

export default memo(({ handleMenuIconClick }) => {
  return (
    <AppBar position="static">
      <Toolbar>
        <IconButton
          color="inherit"
          aria-label="Menu"
          onClick={handleMenuIconClick}
        >
          <MenuIcon />
        </IconButton>
        <Typography variant="h6" color="inherit">
          {"Chess"}
        </Typography>
      </Toolbar>
    </AppBar>
  );
});
