export default {
  "btn.more": "more",

  "ChessDetailBoardCard.upper_player_depth": "Depth: {upper_player_depth}",
  "ChessDetailBoardCard.upper_player_score": "Score: {upper_player_score}",

  "ChessListView.-modified_at": "Modified at",
  "ChessListView.-upper_player_depth": "Depth",
  "ChessListView.-upper_player_score": "Score",

  "ErrorContent.title.400": "Invalid",
  "ErrorContent.title.401": "Unauthorized Access",
  "ErrorContent.title.403": "Forbidden. Invalid Request",
  "ErrorContent.title.404": "Page Not Found",
  "ErrorContent.title.500": "Server Error",

  "ErrorContent.content.400":
    "It seems that application is mal-functioning. We are sorry for that.",
  "ErrorContent.content.401":
    "Your request is not permitted. Please try login using another user or consult your administrator",
  "ErrorContent.content.403":
    "Your request is not permitted. Please try login using another user or consult your administrator",
  "ErrorContent.content.404":
    "You are requesting some content that were either removed or unexist.",
  "ErrorContent.content.500":
    "We are experiencing error internally. This may due to high traffic or other exceptions.",

  "LeftDrawer./chess/play/": "Play",
  "LeftDrawer./chess/list/": "Boards"
};
