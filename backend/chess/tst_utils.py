from django.test import TestCase

from .chs_utils import (
    get_flipped_board,
    get_flipped_piece,
)

from .chs_prefixes import CHS_EMPTY


class ChessUtilsTestCase(TestCase):
    def test_get_flipped_piece(self):
        self.assertEqual(get_flipped_piece('e'), 'E')
        self.assertEqual(get_flipped_piece('j'), 'J')
        self.assertEqual(get_flipped_piece(CHS_EMPTY), CHS_EMPTY)

    def test_get_flipped_board(self):
        board = [
            ['a', 'B', 'c'],
            ['E', 'e', CHS_EMPTY],
            [CHS_EMPTY, 'f', 'G'],
        ]
        flipped = get_flipped_board(board)
        self.assertEqual(len(board), 3)
        self.assertListEqual(board[0], ['a', 'B', 'c'])
        self.assertListEqual(board[1], ['E', 'e', CHS_EMPTY])
        self.assertListEqual(board[2], [CHS_EMPTY, 'f', 'G'])

        self.assertEqual(len(flipped), 3)
        self.assertListEqual(flipped[2], ['A', 'b', 'C'])
        self.assertListEqual(flipped[1], ['e', 'E', CHS_EMPTY])
        self.assertListEqual(flipped[0], [CHS_EMPTY, 'F', 'g'])
