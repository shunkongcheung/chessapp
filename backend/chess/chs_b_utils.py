from .chs_prefixes import CHS_EMPTY


def get_connected_empty_positions(board, cur_position, direction):
    next_position = (
        cur_position[0] + direction[0],
        cur_position[1] + direction[1],
    )
    empty_positions = []

    height = len(board)
    width = len(board[0])if height else 0

    while get_is_position_in_bound(next_position, width=width, height=height):
        if board[next_position[0]][next_position[1]] == CHS_EMPTY:
            empty_positions.append(next_position)
            next_position = (
                next_position[0] + direction[0],
                next_position[1] + direction[1],
            )
        else:
            break
    return empty_positions


def get_is_piece_empty(piece):
    return piece == CHS_EMPTY


def get_is_piece_friendly(cur_piece, next_piece):
    if get_is_piece_empty(next_piece):
        return False
    return cur_piece.isupper() == next_piece.isupper()


def get_is_piece_opponent(cur_piece, next_piece):
    if get_is_piece_empty(next_piece):
        return False
    return cur_piece.isupper() != next_piece.isupper()


def get_is_position_in_bound(position, left=0, top=0, width=9, height=10):
    if position[1] < left or position[1] >= (left+width):
        return False
    if position[0] < (top) or position[0] >= (top+height):
        return False
    return True
