from django.test import TestCase
from .chs_prefixes import (
    CHS_EMPTY as EM,
    CHS_CANNON as CA,
)
from .chs_b_cannon import get_cannon_next_positions


class ChessGetCannonNextPositionTestCase(TestCase):
    def test_get_cannon_next_positions(self):
        UC = CA.upper()
        LC = CA.lower()
        board = [
            [EM, EM, EM, EM, EM],
            [UC, EM, EM, EM, EM],
            [UC, EM, LC, EM, UC],
            [EM, EM, EM, EM, EM],
            [UC, EM, EM, EM, EM],
            [EM, EM, EM, EM, EM],
            [LC, EM, EM, EM, EM],
            [EM, EM, EM, EM, EM],
        ]

        positions = get_cannon_next_positions(board, (2, 0))
        expected = [(2, 1),  (3, 0), (6, 0)]

        positions.sort()
        expected.sort()

        self.assertListEqual(positions, expected)
