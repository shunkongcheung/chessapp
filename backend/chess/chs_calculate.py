from django.contrib.auth.models import User

from .chs_board import get_all_next_boards
from .chs_prefixes import (
    CHS_EMPTY,
    CHS_CANNON,
    CHS_CASTLE,
    CHS_GENERAL,
    CHS_HORSE,
    CHS_JUMBO,
    CHS_KNIGHT,
    CHS_SOLDIER,
)
from .chs_utils import (
    get_flipped_board,
)
from .models import ChessBoardMaster

import asyncio


async def get_board_best_score(
        board, depth,
        is_store_result=False,
        is_create_result=False,
        is_recursive=True
):
    '''
    calculate the best score for the upper player 
    Args:
        board (arrays): to represent the row and columns of chess board
        depth (int): current depth. recursive ending condition
        is_store_result (bool): calculated results should go into database

    Return:
        score (integer): higher score for better board of upper user. vis versa
    '''

    # get winner. if exist return score
    winner, board_score = get_board_winner_and_score(board)
    if winner != CHS_EMPTY or depth <= 0:
        if is_store_result:
            store_board_result(board, board_score, depth, is_create_result)
        return board_score, depth

    # query existing board in database
    r_score, r_depth = retrieve_existing_board_score(board, depth)
    if r_score is not None or not is_recursive:
        return r_score, r_depth

    # get all possible move of myuser
    next_boards = [
        get_flipped_board(board)
        for board in get_all_next_boards(board, True)
    ]

    # pass on to next layer
    calculates = [
        asyncio.create_task(
            get_board_best_score(
                n_board, depth - 1,
                is_store_result, is_create_result
            )
        )
        for n_board in next_boards
    ]

    # since lower layer treated itselves as top player, flip the score back
    results = [await calculate for calculate in calculates]
    results = [(-score, depth) for score, depth in results]
    best_result = max(results)
    best_result = best_result[0], max(depth, best_result[1] + 1)

    # return most flavourable score
    if is_store_result:
        score, depth = best_result
        store_board_result(board, score, depth, is_create_result)

    return best_result


def get_board_winner_and_score(board):
    total, winner_score = 0, 10000
    is_tgeneral_exist, is_bgeneral_exist = False, False
    for row in board:
        for piece in row:
            total += get_piece_score(piece)
            if piece == CHS_GENERAL.upper():
                is_tgeneral_exist = True
            if piece == CHS_GENERAL.lower():
                is_bgeneral_exist = True

    if not is_tgeneral_exist:
        return CHS_GENERAL.lower(), -winner_score + total
    if not is_bgeneral_exist:
        return CHS_GENERAL.upper(), winner_score + total

    return CHS_EMPTY, total


def get_board_hash(board):
    hash_rows = [''.join(row) for row in board]
    return ''.join(hash_rows)


def get_piece_score(piece_prefix):
    piece_prefix_upper = piece_prefix.upper()
    piece_score = [
        CHS_EMPTY.upper(),
        CHS_SOLDIER.upper(),
        CHS_JUMBO.upper(),
        CHS_KNIGHT.upper(),
        CHS_CANNON.upper(),
        CHS_HORSE.upper(),
        CHS_CASTLE.upper(),
        CHS_GENERAL.upper(),
    ]
    piece_index = piece_score.index(piece_prefix_upper)
    return piece_index if piece_prefix.isupper() else -piece_index


def retrieve_existing_board_score(board, depth):
    board_hash = get_board_hash(board)
    qs = ChessBoardMaster.objects.filter(
        board_hash=board_hash,
        disabled_at__isnull=True,
        upper_player_depth__gte=depth
    )
    if not qs.count():
        return None, -1

    board_master = qs.first()
    return board_master.upper_player_score, board_master.upper_player_depth


def store_board_result(board, best_score, depth, is_create_result=True):
    board_hash = get_board_hash(board)

    # ensure one board_master object of this hash exist in database
    qs = ChessBoardMaster.objects.filter(board_hash=board_hash)
    if not qs.count():
        if not is_create_result:
            return
        admin = User.objects.get(username='admin')
        board_master = ChessBoardMaster.objects.create(
            board_hash=board_hash,
            created_by=admin
        )
    else:
        board_master = qs.first()

    # get existing depth and compare
    existing_depth = board_master.upper_player_depth
    if existing_depth > depth:
        return

    # update depth and score
    board_master.upper_player_depth = depth
    board_master.upper_player_score = best_score

    board_master.save()
