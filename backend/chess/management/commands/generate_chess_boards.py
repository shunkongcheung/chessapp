from django.contrib.auth.models import User
from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

from chess.chs_calculate import (
    get_board_best_score,
    get_board_hash,
)
from chess.chs_utils import (
    get_initial_board,
)
from chess.models import ChessBoardMaster

import asyncio
import sys


class Command(BaseCommand):
    help = 'Generate chess board master data for faster calculation'

    def add_arguments(self, parser):
        parser.add_argument(
            '--chunk_size',
            default=10,
            help='decided the speed of generation.',
            type=int,
        )
        parser.add_argument(
            '--depth',
            default=5,
            help='decided the quality of records generated.',
            type=int,
        )
        parser.add_argument(
            '--is_link',
            default=False,
            help='link data only',
            action='store_true'
        )

    def get_chunks(self, split_list, chunk_size):
        return [
            split_list[i:i + chunk_size]
            for i in range(0, len(split_list), chunk_size)
        ]

    def get_diff_time_str(self, cur_time, start_time):
        seconds = (cur_time - start_time).total_seconds()
        hours = int(seconds // 3600)
        minutes = int((seconds % 3600) // 60)
        seconds = int(seconds % 60)
        return f'{hours:02d}:{minutes:02d}:{seconds:02d}'

    def generate_initial_data(self):
        init_board_hash = get_board_hash(get_initial_board())
        qs = ChessBoardMaster.objects.filter(board_hash=init_board_hash)
        if qs.count():
            board_master = qs.first()
            board_master.upper_player_depth = 0
            board_master.save()
            print('generate_initial_data: already contains data. skip. ')
            return

        admin = User.objects.get(username='admin')
        ChessBoardMaster.objects.create(
            board_hash=init_board_hash,
            upper_player_depth=0,
            created_by=admin
        )
        print('generate_initial_data: finished generating initial board master.')

    def increment_data_depth(self, chunk_size, depth):
        start_time = timezone.localtime()
        for idx in range(depth):
            print(
                f'increment_data_depth: beginning incr {idx}/{depth}...'
            )
            for decr in reversed(range(1, idx)):
                ChessBoardMaster.objects\
                    .filter(upper_player_depth=decr)\
                    .update(upper_player_depth=decr + 1)

            qs = ChessBoardMaster.objects\
                .filter(upper_player_depth=0)

            asyncio.run(self.write_data_for_queryset(
                qs, chunk_size, 1, start_time))

            for decr in reversed(range(2, idx+1)):
                qs = ChessBoardMaster.objects\
                    .filter(upper_player_depth=decr)
                asynic.run(self.write_data_for_queryset(
                    qs, qs.count(), decr, start_time
                ))

    def link_existing_data(self, chunk_size):
        start_time = timezone.localtime()
        top_depth = ChessBoardMaster.objects\
            .order_by('-upper_player_depth')\
            .first()\
            .upper_player_depth

        for idx in range(top_depth):
            print(
                f'link_existing_data: beginning incr {idx}/{top_depth}...'
            )
            asyncio.run(self.write_data_for_queryset(
                ChessBoardMaster.objects.all(), chunk_size, 1, start_time, False
            ))

    def print_info(self, idx, prev_count, start_time, qs_count):
        cur_count = ChessBoardMaster.objects.count()
        diff_time_str = self.get_diff_time_str(
            timezone.localtime(),
            start_time
        )
        sys.stdout.write("\033[F")
        print(
            f'write_data_for_layer: {diff_time_str} {idx}/{qs_count} completed. '
            f'db contains {cur_count}(+{cur_count - prev_count}) record(s)..'
        )
        return cur_count

    async def write_data_for_queryset(
            self, c_queryset, chunk_size, depth, start_time, is_create=True
    ):
        qs_count = c_queryset.count()
        prev_count = ChessBoardMaster.objects.all().count()
        chunks = self.get_chunks(c_queryset, chunk_size)

        print()
        for idx, board_masters in enumerate(chunks):
            tasks = []
            for board_master in board_masters:
                board = board_master.get_board_from_hash()
                tasks.append(self.write_data_from_board(
                    board, depth, is_create
                ))
            for task in tasks:
                await task

            idx = (idx+1) * chunk_size
            prev_count = self.print_info(idx, prev_count, start_time, qs_count)

    def write_data_from_board(self, board, depth, is_create):
        is_store_result, is_recursive = True, True
        return asyncio.create_task(
            get_board_best_score(
                board, depth, is_store_result, is_create, is_recursive
            )
        )

    def handle(self, *args, **options):
        chunk_size, depth = options['chunk_size'], options['depth']
        is_link = options['is_link']

        print()
        print()
        print(
            f'generate_chess_board: begin [chunk_size {chunk_size}] ' +
            f'[depth {depth}] [clear {is_clear_belows}]....'
        )
        print(f'generate_chess_board: [is_link {is_link}] ')

        self.generate_initial_data()
        if is_link:
            self.link_existing_data(chunk_size)
        else:
            self.increment_data_depth(chunk_size, depth)

        print('generate_chess_board: finish execution. exit without error.')
        print()
        print()
