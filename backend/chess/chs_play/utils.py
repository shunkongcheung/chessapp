from asyncio import create_task
from random import randint

from chess.chs_board import get_all_next_boards
from chess.chs_calculate import get_board_best_score


async def get_next_board(board, depth, difficulty, is_recursive=False):
    next_boards = get_all_next_boards(board, True)
    calculates = [
        create_task(
            get_board_best_score(n_board, depth, False, False, is_recursive)
        )
        for n_board in next_boards
    ]
    results = [await calculate for calculate in calculates]
    scores = [-score for score, _ in results]

    # since get_board_best_score is may be forced to run non-recursively,
    # no conclusive result could be drawn if boards are indeterministic.
    if None in scores:
        return None

    pair = get_score_board_pair(
        list(zip(scores, next_boards)),
        get_is_play_dumb(difficulty)
    )
    return pair[1]


def get_is_play_dumb(difficulty):
    if difficulty < 1 or difficulty > 4:
        raise Exception(
            'get_is_play_dumb: expecting difficulity of value 1-4 ' +
            f'receiving difficulity value of {difficulity}.'
        )

    # distinguish probablity value
    # probablity of 10 % (3+1) and 10 % (4+1) == 0 are both 0.2
    # probablity of 10 % (5+1) == 0 is 0.1
    difficulty = 5 if difficulty == 4 else difficulty
    return randint(1, 10) % (difficulty+1) == 0


def get_score_board_pair(score_board_pairs, is_play_dumb):
    # always get score board pair for upper player

    scores = [x[0] for x in score_board_pairs]
    sorted_scores = list(set(scores))
    sorted_scores.sort()

    desire_index = -2 if is_play_dumb else -1
    desire_score = sorted_scores[desire_index]

    match_board_pairs = [
        pair for pair in score_board_pairs
        if pair[0] == desire_score
    ]
    desire_index = randint(0, len(match_board_pairs) - 1)
    return match_board_pairs[desire_index]
