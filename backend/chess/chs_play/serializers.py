from django.utils.translation import ugettext as _
from rest_framework.exceptions import ValidationError
from rest_framework.serializers import (
    BooleanField,
    CharField,
    ChoiceField,
    JSONField,
    IntegerField,
    ListField,
    ModelSerializer,
    Serializer,
)
from chess.chs_prefixes import (
    CHS_EMPTY,
    CHS_GENERAL,
)
from chess.chs_board import (
    get_all_next_boards,
)
from chess.models import (
    ChessBoardMaster,
)


class ChessPlaySerializer(Serializer):
    board = ListField(child=ListField(child=CharField()))
    difficulity = IntegerField(min_value=1, max_value=4)
    is_calculate_upper_side = BooleanField()

    winner = ChoiceField(
        choices=[
            (CHS_EMPTY, _('Draw')),
            (CHS_GENERAL.upper(), _('Top Player')),
            (CHS_GENERAL.lower(), _('Bottom Player'))
        ],
        default='draw',
        read_only=True,
    )
    approx_wait_seconds = IntegerField(default=-1, read_only=True)
    queue_id = IntegerField(default=-1, read_only=True)


class ChessGetValidateMoveSerializer(Serializer):
    from_board = ListField(child=ListField(child=CharField()))
    is_upper_side = BooleanField()

    valid_moves = ListField(
        child=ListField(child=JSONField()),
        read_only=True
    )

    def get_diff_coords(self, board_a, board_b):
        diff_coords = []
        for row_idx, (row_a, row_b) in enumerate(zip(board_a, board_b)):
            for col_idx, (piece_a, piece_b) in enumerate(zip(row_a, row_b)):
                if piece_a != piece_b:
                    diff_coords.append({'row': row_idx, 'col': col_idx})
        return diff_coords

    def get_move_from_boards(self, from_board, to_board):
        '''
        Yes. i know this is crazy, get_all_next_boards should knew the move.
        But for design/performance reason, it is returning the board instead of 
        the actual move. So we need to revert the move back here
        '''
        diff_coords = self.get_diff_coords(from_board, to_board)
        f_coord, s_coord = diff_coords
        if to_board[f_coord['row']][f_coord['col']] == CHS_EMPTY:
            from_coord, to_coord = f_coord, s_coord
        else:
            from_coord, to_coord = s_coord, f_coord
        return from_coord, to_coord

    def get_valid_moves(self, from_board, is_upper_side):
        next_boards = get_all_next_boards(
            from_board, is_upper_side
        )
        return [
            self.get_move_from_boards(from_board, to_board)
            for to_board in next_boards
        ]

    def validate(self, values):
        from_board, is_upper_side = values['from_board'], values['is_upper_side']
        next_boards = get_all_next_boards(from_board, is_upper_side)
        values['valid_moves'] = self.get_valid_moves(
            from_board, is_upper_side
        )
        return values


class ChessInitBoardSerializer(Serializer):
    board = ListField(child=ListField(child=CharField()))
