from base.apis import (
    MyCreateAPIView,
    MyListAPIView,
    MyObjectAPIView,
)

from chess.chs_calculate import (
    get_board_winner_and_score,
)
from chess.chs_prefixes import (
    CHS_EMPTY,
)
from chess.chs_utils import (
    get_initial_board,
    get_flipped_board,
)
from chess.models import (
    ChessBoardMaster,
)

from .utils import (
    get_next_board,
)
from .serializers import (
    ChessPlaySerializer,
    ChessInitBoardSerializer,
    ChessGetValidateMoveSerializer,
)

import asyncio


class ChessPlayAPIView(MyCreateAPIView):
    serializer_class = ChessPlaySerializer

    def perform_create(self, serializer):
        board = serializer.validated_data['board']
        difficulty = serializer.validated_data['difficulity']
        is_upper_side = serializer.validated_data['is_calculate_upper_side']

        winner, _ = get_board_winner_and_score(board)
        serializer.validated_data['winner'] = winner

        if winner != CHS_EMPTY:
            return

        if not is_upper_side:
            board = get_flipped_board(board)

        cal_board = asyncio.run(get_next_board(
            board, 1, difficulty, is_upper_side
        ))
        serializer.validated_data['board'] = cal_board


class ChessGetValidateMoveAPIView(MyCreateAPIView):
    serializer_class = ChessGetValidateMoveSerializer

    def perform_create(self, serializer):
        pass


class ChessInitBoardAPIView(MyObjectAPIView):
    serializer_class = ChessInitBoardSerializer

    def get_object(self):
        return {'board': get_initial_board()}
