from django.urls import path
from .apis import (
    ChessPlayAPIView,
    ChessInitBoardAPIView,
    ChessGetValidateMoveAPIView,
)

urlpatterns = [
    path('play/', ChessPlayAPIView.as_view()),
    path('init_board/', ChessInitBoardAPIView.as_view()),
    path('get_valid_moves/', ChessGetValidateMoveAPIView.as_view()),
]
