from base.models import BaseModel
from django.db import models


class ChessBoardMaster(BaseModel):
    board_hash = models.CharField(max_length=90)
    upper_player_score = models.IntegerField(default=0)
    upper_player_depth = models.IntegerField(default=-1)

    def __str__(self):
        return self.board_hash

    def pretty_print(self):
        uscore, udepth = self.upper_player_score, self.upper_player_depth

        print('=============================================')
        print(
            f'================= [{uscore:2d}, {udepth:2d}] =================='
        )
        for idx, row in enumerate(self.get_board_from_hash()):
            if idx == 5:
                print()
            print(row)
        print('=============================================')
        print('=============================================')
        print()

    def get_board_from_hash(self):
        board_hash = self.board_hash
        chars = list(board_hash)
        return [chars[i*9:(i+1)*9] for i in range(10)]
