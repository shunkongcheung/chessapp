from rest_framework.serializers import (
    ListField,
    ModelSerializer,
)
from .models import (
    ChessBoardMaster,
)


class ChessBoardMasterBoardField(ListField):
    def get_attribute(self, obj):
        return obj

    def to_representation(self, value):
        return value.get_board_from_hash()


class ChessBoardMasterSerializer(ModelSerializer):
    board = ChessBoardMasterBoardField()

    class Meta:
        model = ChessBoardMaster
        fields = [
            'id', 'board', 'board_hash',
            'upper_player_depth', 'upper_player_score',
        ]
