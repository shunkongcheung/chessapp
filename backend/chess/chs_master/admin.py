from base.admin import BaseAdmin
from django.contrib import admin

from .models import (
    ChessBoardMaster,
)


class ChessBoardMasterAdmin(BaseAdmin):
    list_display = ('board_hash', 'upper_player_depth', 'upper_player_score', )


admin.site.register(ChessBoardMaster, ChessBoardMasterAdmin)
