from base.apis import (
    MyListAPIView,
    MyObjectAPIView,
)

from .models import (
    ChessBoardMaster,
)
from .serializers import (
    ChessBoardMasterSerializer,
)


class ChessBoardMasterListAPIView(MyListAPIView):
    model = ChessBoardMaster
    serializer_class = ChessBoardMasterSerializer

    def get_queryset(self):
        return self.model.objects.all()


class ChessBoardMasterObjectAPIView(MyObjectAPIView):
    http_method_names = ['get', 'options', ]
    model = ChessBoardMaster
    serializer_class = ChessBoardMasterSerializer

    def get_queryset(self):
        return self.model.objects.all()
