from django.urls import path
from .apis import (
    ChessBoardMasterObjectAPIView,
    ChessBoardMasterListAPIView,
)

urlpatterns = [
    path('list/', ChessBoardMasterListAPIView.as_view()),
    path('<int:pk>/', ChessBoardMasterObjectAPIView.as_view()),
]
