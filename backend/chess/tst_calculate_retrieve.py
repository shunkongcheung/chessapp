from django.contrib.auth.models import User
from django.test import TestCase

from .chs_calculate import (
    get_board_hash,
    retrieve_existing_board_score,
)
from .chs_prefixes import (
    CHS_GENERAL as CG,
    CHS_CASTLE as CC,
    CHS_HORSE as CH,
    CHS_CANNON as CO,
    CHS_KNIGHT as CK,
    CHS_JUMBO as CJ,
    CHS_SOLDIER as CS,
    CHS_EMPTY as CM,
)
from .models import ChessBoardMaster


class ChessCalculateRetrieveTestCase(TestCase):
    board_1 = [
        [CG, CM, CM, CS],
        [CG.lower(), CM, CM, CS.lower()],
    ]
    board_2 = [
        [CG, CM, CM, CK],
        [CG.lower(), CM, CM, CS.lower()],
    ]

    def setUp(self):
        user = User.objects.create(username='admin')
        ChessBoardMaster.objects.create(
            board_hash=get_board_hash(self.board_1),
            upper_player_score=10,
            upper_player_depth=5,
            created_by=user
        )

    def tearDown(self):
        ChessBoardMaster.objects.get(
            board_hash=get_board_hash(self.board_1)
        ).delete()

    def test_retieve_existing_board_score_same_depth(self):
        self.assertEqual(
            retrieve_existing_board_score(self.board_1, 5)[0],
            10
        )

    def test_retieve_existing_board_score_shallow(self):
        self.assertEqual(
            retrieve_existing_board_score(self.board_1, 3)[0],
            10
        )

    def test_retieve_existing_board_score_depper(self):
        self.assertIsNone(
            retrieve_existing_board_score(self.board_1, 6)[0]
        )

    def test_retieve_existing_board_score_not_exist(self):
        self.assertIsNone(
            retrieve_existing_board_score(self.board_2, -1)[0]
        )
