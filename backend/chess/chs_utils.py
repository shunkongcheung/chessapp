from .chs_prefixes import (
    CHS_EMPTY as EMY,
    CHS_CANNON as CN,
    CHS_CASTLE as CE,
    CHS_GENERAL as GL,
    CHS_HORSE as HE,
    CHS_JUMBO as JO,
    CHS_KNIGHT as KT,
    CHS_SOLDIER as SR,
)


def get_flipped_board(board):
    flipped_board = [
        [get_flipped_piece(piece) for piece in row]
        for row in reversed(board)
    ]
    return flipped_board


def get_flipped_piece(piece):
    if piece == EMY:
        return piece
    elif piece.isupper():
        return piece.lower()
    else:
        return piece.upper()


def get_hash_from_board(board):
    hash_rows = [''.join(row) for row in board]
    return ''.join(hash_rows)


def get_initial_board():
    CNU, CNL = CN.upper(), CN.lower()
    CEU, CEL = CE.upper(), CE.lower()
    GLU, GLL = GL.upper(), GL.lower()
    HEU, HEL = HE.upper(), HE.lower()
    JOU, JOL = JO.upper(), JO.lower()
    KTU, KTL = KT.upper(), KT.lower()
    SRU, SRL = SR.upper(), SR.lower()
    return [
        [CEU, HEU, JOU, KTU, GLU, KTU, JOU, HEU, CEU],
        [EMY, EMY, EMY, EMY, EMY, EMY, EMY, EMY, EMY],
        [EMY, CNU, EMY, EMY, EMY, EMY, EMY, CNU, EMY],
        [SRU, EMY, SRU, EMY, SRU, EMY, SRU, EMY, SRU],
        [EMY, EMY, EMY, EMY, EMY, EMY, EMY, EMY, EMY],

        [EMY, EMY, EMY, EMY, EMY, EMY, EMY, EMY, EMY],
        [SRL, EMY, SRL, EMY, SRL, EMY, SRL, EMY, SRL],
        [EMY, CNL, EMY, EMY, EMY, EMY, EMY, CNL, EMY],
        [EMY, EMY, EMY, EMY, EMY, EMY, EMY, EMY, EMY],
        [CEL, HEL, JOL, KTL, GLL, KTL, JOL, HEL, CEL],
    ]


def get_winner(board):
    is_tgeneral_exist, is_bgeneral_exist = False, False
    for row in board:
        for piece in row:
            if piece == GL.upper():
                is_tgeneral_exist = True
            if piece == GL.lower():
                is_bgeneral_exist = True

    if not is_tgeneral_exist:
        return GL.lower()
    if not is_bgeneral_exist:
        return GL.upper()

    return EMY
