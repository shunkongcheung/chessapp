from django.contrib.auth.models import User
from django.test import TestCase

from .chs_calculate import (
    get_board_hash,
    store_board_result,
)

from .chs_prefixes import (
    CHS_GENERAL as CG,
    CHS_CASTLE as CC,
    CHS_HORSE as CH,
    CHS_CANNON as CO,
    CHS_KNIGHT as CK,
    CHS_JUMBO as CJ,
    CHS_SOLDIER as CS,
    CHS_EMPTY as CM,
)
from .models import ChessBoardMaster


class ChessCalculateStoreTestCase(TestCase):
    board_1 = [
        [CG, CM, CM, CJ],
        [CG.lower(), CM, CM, CS.lower()],
    ]
    board_2 = [
        [CG, CM, CM, CO],
        [CG.lower(), CM, CM, CS.lower()],
    ]

    def setUp(self):
        user = User.objects.create(username='admin')
        ChessBoardMaster.objects.create(
            board_hash=get_board_hash(self.board_1),
            upper_player_score=10,
            upper_player_depth=5,
            created_by=user
        )

    def tearDown(self):
        ChessBoardMaster.objects.get(
            board_hash=get_board_hash(self.board_1)
        ).delete()

    def test_store_board_result_new(self):
        qs = ChessBoardMaster.objects.filter(
            board_hash=get_board_hash(self.board_2)
        )
        self.assertEqual(qs.count(), 0)
        store_board_result(self.board_2, 100, 3)
        self.assertEqual(qs.count(), 1)

    def test_store_board_result_upper_depper(self):
        qs = ChessBoardMaster.objects.get(
            board_hash=get_board_hash(self.board_1))
        self.assertEqual(qs.upper_player_score, 10)
        self.assertEqual(qs.upper_player_depth, 5)

        store_board_result(self.board_1, 100, 6)
        qs = ChessBoardMaster.objects.get(
            board_hash=get_board_hash(self.board_1)
        )
        self.assertEqual(qs.upper_player_score, 100)
        self.assertEqual(qs.upper_player_depth, 6)

        qs.upper_player_score = 10
        qs.upper_player_depth = 5
        qs.save()

    def test_store_board_result_upper_shallow(self):
        qs = ChessBoardMaster.objects.get(
            board_hash=get_board_hash(self.board_1)
        )
        self.assertEqual(qs.upper_player_score, 10)
        self.assertEqual(qs.upper_player_depth, 5)

        store_board_result(self.board_1, 100, 3)
        qs = ChessBoardMaster.objects.get(
            board_hash=get_board_hash(self.board_1))
        self.assertEqual(qs.upper_player_score, 10)
        self.assertEqual(qs.upper_player_depth, 5)

    def test_store_board_result_upper_same_depth(self):
        qs = ChessBoardMaster.objects.get(
            board_hash=get_board_hash(self.board_1))
        self.assertEqual(qs.upper_player_score, 10)
        self.assertEqual(qs.upper_player_depth, 5)

        store_board_result(self.board_1, 100, 5)
        qs = ChessBoardMaster.objects.get(
            board_hash=get_board_hash(self.board_1))
        self.assertEqual(qs.upper_player_score, 100)
        self.assertEqual(qs.upper_player_depth, 5)
