from django.test import TestCase
from .chs_board import get_position_with_friendly_pieces
from .chs_prefixes import CHS_EMPTY


class ChessGetPositionWithFriendlyPiecesTestCase(TestCase):
    board = [
        ['a', 'B', 'c'],
        [CHS_EMPTY, 'E', 'F'],
    ]
    expected_lower = [(0, 0), (0, 2)]
    expected_upper = [(0, 1), (1, 1), (1, 2)]

    def test_get_position_with_friendly_pieces(self):
        lower_positions = get_position_with_friendly_pieces(
            self.board, False
        )
        self.assertListEqual(lower_positions, self.expected_lower)

        upper_positions = get_position_with_friendly_pieces(
            self.board, True
        )
        self.assertListEqual(upper_positions, self.expected_upper)
