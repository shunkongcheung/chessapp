from django.test import TestCase

from .chs_calculate import (
    get_board_winner_and_score,
    get_piece_score,
)
from .chs_prefixes import (
    CHS_GENERAL as CG,
    CHS_CASTLE as CC,
    CHS_HORSE as CH,
    CHS_CANNON as CO,
    CHS_KNIGHT as CK,
    CHS_JUMBO as CJ,
    CHS_SOLDIER as CS,
    CHS_EMPTY as CM,
)


class ChessCalculateTestCase(TestCase):
    def test_get_board_winner_and_score(self):
        UG, UK, LS, LG = CG.upper(), CK.upper(), CS.lower(), CG.lower()
        draw_board = [
            [UG, CM, UK, CM],
            [CM, CM, LS, CM],
            [CM, CM, CM, LG],
        ]
        self.assertEqual(get_board_winner_and_score(draw_board)[1], 2)

    def test_get_board_winner_and_score_lwin(self):
        UG, UK, LS, LG = CG.upper(), CK.upper(), CS.lower(), CG.lower()
        u_board = [
            [CM, CM, LS, CM],
            [CM, CM, CM, LG],
        ]
        self.assertEqual(get_board_winner_and_score(u_board)[0], LG)

    def test_get_board_winner_and_score_uwin(self):
        UG, UK, LS, LG = CG.upper(), CK.upper(), CS.lower(), CG.lower()
        l_board = [
            [UG, CM, UK, CM],
            [CM, CM, LS, CM],
        ]
        self.assertEqual(get_board_winner_and_score(l_board)[0], UG)

    def test_get_piece_score(self):
        self.assertEqual(get_piece_score(CM.upper()), 0)

        self.assertEqual(get_piece_score(CS.upper()), 1)
        self.assertEqual(get_piece_score(CJ.upper()), 2)
        self.assertEqual(get_piece_score(CK.upper()), 3)
        self.assertEqual(get_piece_score(CO.upper()), 4)
        self.assertEqual(get_piece_score(CH.upper()), 5)
        self.assertEqual(get_piece_score(CC.upper()), 6)
        self.assertEqual(get_piece_score(CG.upper()), 7)

        self.assertEqual(get_piece_score(CS.lower()), -1)
        self.assertEqual(get_piece_score(CJ.lower()), -2)
        self.assertEqual(get_piece_score(CK.lower()), -3)
        self.assertEqual(get_piece_score(CO.lower()), -4)
        self.assertEqual(get_piece_score(CH.lower()), -5)
        self.assertEqual(get_piece_score(CC.lower()), -6)
        self.assertEqual(get_piece_score(CG.lower()), -7)
