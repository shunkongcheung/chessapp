from django.test import TestCase
from .chs_prefixes import (
    CHS_EMPTY as EM,
    CHS_JUMBO as JU,
)
from .chs_b_jumbo import get_jumbo_next_positions


class ChessGetJumboNextPositionTestCase(TestCase):
    def get_test_board(self):
        UJ = JU.upper()
        LJ = JU.lower()
        return [
            [EM, UJ, EM, EM, EM, EM, EM, EM, EM],
            [EM, EM, EM, EM, LJ, EM, EM, EM, EM],
            [EM, EM, EM, EM, EM, EM, EM, EM, EM],
            [EM, EM, EM, EM, EM, EM, UJ, EM, EM],
            [EM, EM, EM, EM, EM, EM, EM, EM, EM],

            [EM, EM, EM, EM, EM, EM, EM, EM, EM],
            [EM, EM, LJ, EM, EM, EM, EM, EM, EM],
            [EM, EM, EM, EM, EM, EM, EM, EM, EM],
            [EM, EM, EM, EM, UJ, EM, EM, EM, EM],
            [EM, EM, EM, EM, EM, EM, EM, LJ, EM],
        ]

    def test_get_jumbo_next_positions_lower1(self):
        board = self.get_test_board()

        positions = get_jumbo_next_positions(board, (6, 2))
        expected = [(8, 0), (8, 4)]

        positions.sort()
        expected.sort()
        self.assertListEqual(positions, expected)

    def test_get_jumbo_next_positions_lower2(self):
        board = self.get_test_board()

        positions = get_jumbo_next_positions(board, (9, 8))
        expected = [(7, 6)]

        positions.sort()
        expected.sort()
        self.assertListEqual(positions, expected)

    def test_get_jumbo_next_positions_upper1(self):
        board = self.get_test_board()

        positions = get_jumbo_next_positions(board, (3, 6))
        expected = [(1, 4), (1, 8)]

        positions.sort()
        expected.sort()
        self.assertListEqual(positions, expected)

    def test_get_jumbo_next_positions_upper2(self):
        board = self.get_test_board()

        positions = get_jumbo_next_positions(board, (0, 1))
        expected = [(2, 3)]

        positions.sort()
        expected.sort()
        self.assertListEqual(positions, expected)
