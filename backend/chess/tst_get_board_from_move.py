from django.test import TestCase
from .chs_board import (
    get_board_from_move,
)
from .chs_prefixes import CHS_EMPTY

# Create your tests here.


class ChessGetBoardFromMoveTestCase(TestCase):
    expected_ori = [
        ['1', '2', '3'],
        ['a', 'b', 'c'],
    ]
    expected_moved = [
        ['1', CHS_EMPTY, '3'],
        ['a', 'b', '2'],
    ]

    def assert_board_size(self, board):
        self.assertEqual(len(board), 2)
        self.assertEqual(len(board[0]), 3)
        self.assertEqual(len(board[1]), 3)

    def assert_board_value(self, outcome_board, expected_board):
        self.assertListEqual(outcome_board[0], expected_board[0])
        self.assertListEqual(outcome_board[1], expected_board[1])

    def test_get_board_from_move(self):
        ori_board = [
            ['1', '2', '3'],
            ['a', 'b', 'c'],
        ]
        moved_board = get_board_from_move(
            ori_board, (0, 1), (1, 2)
        )

        self.assert_board_size(ori_board)
        self.assert_board_value(ori_board, self.expected_ori)

        self.assert_board_size(moved_board)
        self.assert_board_value(moved_board, self.expected_moved)
