from django.urls import include, path

from .chs_master import urls as chs_master_urls
from .chs_play import urls as chs_play_urls

urlpatterns = [
    path('chs_master/', include(chs_master_urls)),
    path('chs_play/', include(chs_play_urls)),
]
