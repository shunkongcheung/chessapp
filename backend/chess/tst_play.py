from django.test import TestCase
from random import shuffle

from .chs_play.utils import (
    get_is_play_dumb,
    get_score_board_pair,
)


class ChessPlayTestCase(TestCase):
    score_board_pairs = [
        (0, 'low'),
        (0, 'low'),
        (1, 'second'),
        (2, 'third'),
        (3, 'high'),
    ]

    def setUp(self):
        is_continue = True
        while is_continue:
            shuffle(self.score_board_pairs)
            is_start_low = self.score_board_pairs[0][1] == 'low'
            is_end_high = self.score_board_pairs[-1][1] == 'high'
            is_continue = is_start_low and is_end_high

    def get_play_dumb_percent(self, difficulty):
        dumb_total, times = 0, 10000
        for _ in range(times):
            if get_is_play_dumb(difficulty):
                dumb_total += 1
        return 1.0 * dumb_total / times

    def test_get_is_play_dumb(self):
        margin = 0.05
        self.assertLess(abs(0.500 - self.get_play_dumb_percent(1)), margin)
        self.assertLess(abs(0.333 - self.get_play_dumb_percent(2)), margin)
        self.assertLess(abs(0.200 - self.get_play_dumb_percent(3)), margin)
        self.assertLess(abs(0.100 - self.get_play_dumb_percent(4)), margin)

    def test_get_score_board_pair_find(self):
        res = get_score_board_pair(self.score_board_pairs, False)
        self.assertEqual(res[0], 3)

    def test_get_score_board_pair_find_dumb(self):
        res = get_score_board_pair(self.score_board_pairs, True)
        self.assertEqual(res[0], 2)
