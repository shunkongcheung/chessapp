from django.test import TestCase
from .chs_prefixes import CHS_EMPTY
from .chs_b_utils import get_connected_empty_positions
# Create your tests here.

EMP = CHS_EMPTY


class ChessGetConnectedEmptyPositions(TestCase):
    board = [
        [EMP, EMP, EMP, EMP, EMP, EMP, EMP],
        [EMP, EMP, EMP, EMP, EMP, EMP, EMP],
        [EMP, EMP, 'A', 'A', 'A', EMP, EMP],
        [EMP, EMP, 'A', EMP, 'A', EMP, EMP],
        [EMP, EMP, 'A', 'A', 'A', EMP, EMP],
        [EMP, EMP, EMP, EMP, EMP, EMP, EMP],
        [EMP, EMP, EMP, EMP, EMP, EMP, EMP],
    ]

    left = (0, -1)
    right = (0, 1)

    up = (-1, 0)
    down = (1, 0)

    bottom_left = (1, -1)
    bottom_right = (1, 1)

    top_left = (-1, -1)
    top_right = (-1, 1)

    def assert_positions(self, spot, direction, expected):
        self.assertListEqual(
            self.get_connected_empty_positions(
                spot, direction
            ),
            expected
        )

    def get_all_directions(self):
        return[
            self.left, self.right, self.up, self.down,
            self.bottom_left, self.bottom_right,
            self.top_left, self.top_right
        ]

    def get_connected_empty_positions(self, position, direction):
        positions = get_connected_empty_positions(
            self.board,
            position,
            direction
        )
        return positions

    def test_surrounded_spot(self):
        surround_position = (3, 3)
        all_directions = self.get_all_directions()
        for direction in all_directions:
            self.assert_positions(surround_position, direction, [])

    def test_right_blocked_spot(self):
        right_blocked_spot = (3, 1)
        right_directions = [self.right, self.top_right, self.bottom_right]
        for direction in right_directions:
            self.assert_positions(right_blocked_spot, direction, [])

        self.assert_positions(right_blocked_spot, self.left, [(3, 0)])
        self.assert_positions(right_blocked_spot, self.top_left, [(2, 0)])
        self.assert_positions(right_blocked_spot, self.bottom_left, [(4, 0)])

        self.assert_positions(
            right_blocked_spot, self.up,
            [(2, 1), (1, 1), (0, 1)]
        )
        self.assert_positions(
            right_blocked_spot, self.down,
            [(4, 1), (5, 1), (6, 1)]
        )

    def test_left_blocked_spot(self):
        left_blocked_spot = (3, 5)
        left_directions = [self.left, self.top_left, self.bottom_left]
        for direction in left_directions:
            self.assert_positions(left_blocked_spot, direction, [])

        self.assert_positions(left_blocked_spot, self.right, [(3, 6)])
        self.assert_positions(left_blocked_spot, self.top_right, [(2, 6)])
        self.assert_positions(left_blocked_spot, self.bottom_right, [(4, 6)])

        self.assert_positions(
            left_blocked_spot, self.up,
            [(2, 5), (1, 5), (0, 5)]
        )
        self.assert_positions(
            left_blocked_spot, self.down,
            [(4, 5), (5, 5), (6, 5)]
        )

    def test_top_blocked_spot(self):
        top_blocked_spot = (5, 3)
        top_directions = [self.up, self.top_right, self.top_left]
        for direction in top_directions:
            self.assert_positions(top_blocked_spot, direction, [])

        self.assert_positions(top_blocked_spot, self.down, [(6, 3)])
        self.assert_positions(top_blocked_spot, self.bottom_left, [(6, 2)])
        self.assert_positions(top_blocked_spot, self.bottom_right, [(6, 4)])

        self.assert_positions(
            top_blocked_spot, self.left,
            [(5, 2), (5, 1), (5, 0)]
        )
        self.assert_positions(
            top_blocked_spot, self.right,
            [(5, 4), (5, 5), (5, 6)]
        )

    def test_bottom_blocked_spot(self):
        bottom_blocked_spot = (1, 3)
        bottom_directions = [self.down, self.bottom_right, self.bottom_left]
        for direction in bottom_directions:
            self.assert_positions(bottom_blocked_spot, direction, [])

        self.assert_positions(bottom_blocked_spot, self.up, [(0, 3)])
        self.assert_positions(bottom_blocked_spot, self.top_left, [(0, 2)])
        self.assert_positions(bottom_blocked_spot, self.top_right, [(0, 4)])

        self.assert_positions(
            bottom_blocked_spot, self.left,
            [(1, 2), (1, 1), (1, 0)]
        )
        self.assert_positions(
            bottom_blocked_spot, self.right,
            [(1, 4), (1, 5), (1, 6)]
        )
