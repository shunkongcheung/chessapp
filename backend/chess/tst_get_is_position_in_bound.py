from django.test import TestCase
from .chs_b_utils import (
    get_is_position_in_bound,
)


class ChessGetIsPositionInBoundTestCase(TestCase):
    def assert_boundary_within(self):
        self.assertTrue(get_is_position_in_bound((0, 0)))
        self.assertTrue(get_is_position_in_bound((9, 0)))
        self.assertTrue(get_is_position_in_bound((0, 8)))

    def assert_boundary_within_with_input(self):
        left, top, width, height = 2, 3, 4, 5
        self.assertTrue(
            get_is_position_in_bound((top, left), left, top, width, height)
        )
        self.assertTrue(
            get_is_position_in_bound(
                (top+height-1, left), left, top, width, height)
        )
        self.assertTrue(
            get_is_position_in_bound(
                (top, (left+width - 1)), left, top, width, height)
        )

    def assert_boundary_outside(self):
        self.assertFalse(get_is_position_in_bound((-1, 0)))
        self.assertFalse(get_is_position_in_bound((10, 0)))
        self.assertFalse(get_is_position_in_bound((0, -1)))
        self.assertFalse(get_is_position_in_bound((0, 9)))

    def assert_boundary_within_with_input(self):
        left, top, width, height = 2, 3, 4, 5
        self.assertFalse(
            get_is_position_in_bound((top-1, left), left, top, width, height)
        )
        self.assertFalse(
            get_is_position_in_bound(
                (top+heigth, left), left, top, width, height)
        )
        self.assertFalse(
            get_is_position_in_bound((top, left-1), left, top, width, height)
        )
        self.assertFalse(
            get_is_position_in_bound(
                (top, left+width), left, top, width, height)
        )

    def test_get_is_position_in_bound(self):
        self.assert_boundary_within()
        self.assert_boundary_outside()
