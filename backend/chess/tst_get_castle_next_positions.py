from django.test import TestCase
from .chs_prefixes import (
    CHS_EMPTY as EMP,
    CHS_CANNON as CAS,
)
from .chs_b_castle import get_castle_next_positions


class ChessGetCastleNextPositionTestCase(TestCase):
    def test_get_castle_next_positions(self):
        UPC = CAS.upper()
        LOC = CAS.lower()
        board = [
            [EMP, EMP, EMP, EMP],
            [UPC, EMP, EMP, EMP],
            [UPC, EMP, LOC, EMP],
            [EMP, EMP, EMP, EMP],
            [UPC, EMP, EMP, EMP],
            [EMP, EMP, EMP, EMP],
        ]

        positions = get_castle_next_positions(board, (2, 0))
        expected = [(2, 1), (2, 2), (3, 0)]

        positions.sort()
        expected.sort()

        self.assertListEqual(positions, expected)
