from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

# Create your models here.


class AutoDateTimeField(models.DateTimeField):
    def pre_save(self, model_instance, add):
        return timezone.now()


class BaseModel(models.Model):
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(default=timezone.now)
    modified_at = AutoDateTimeField(default=timezone.now)
    disabled_at = models.DateTimeField(blank=True, null=True)

    def __unicode__(self):
        return 'Not Implemented'

    class Meta:
        abstract = True
