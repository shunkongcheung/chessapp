import re


def get_value_in_correct_type(value):
    if ',' in value:
        return value.split(',')

    if re.match(r'\d+\.\d*', value):
        return float(value)

    if re.match(r'\d+', value):
        return int(value)

    return value
