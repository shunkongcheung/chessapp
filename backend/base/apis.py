from __future__ import absolute_import

from django.utils import timezone
from rest_framework.generics import (
    CreateAPIView,
    GenericAPIView,
    ListAPIView,
)
from rest_framework.mixins import (
    DestroyModelMixin,
    RetrieveModelMixin,
    UpdateModelMixin,
)

from .utils import (
    get_value_in_correct_type,
)


class MyBaseAPIView(GenericAPIView):
    is_listview = False

    def get_queryset(self):
        return self.model.objects.filter(
            created_by=self.request.user,
            disabled_at__is_null=True
        )


class MyCreateAPIView(CreateAPIView, MyBaseAPIView):
    http_method_names = ['post', 'options']

    def perform_create(self, serializer):
        serializer.validated_data['created_by'] = self.request.user
        serializer.save()


class MyListAPIView(ListAPIView, MyBaseAPIView):
    http_method_names = ['get', 'options']
    is_listview = True

    def get_filter_exclude_keywords(self):
        return ['order_by', 'page', 'page_size', ]

    def get_filter_params(self):
        exclude_keywords = self.get_filter_exclude_keywords()
        filter_params = {}
        for key in self.request.query_params.keys():
            if key in exclude_keywords:
                continue

            value = self.request.query_params[key]
            filter_params[key] = get_value_in_correct_type(value)

        return filter_params

    def order_queryset(self, queryset):
        order_by = self.request.query_params.get('order_by', '-modified_at')
        return queryset.order_by(order_by, 'id')

    def filter_queryset(self, queryset):
        filter_params = self.get_filter_params()
        queryset = queryset.filter(**filter_params)
        return self.order_queryset(queryset)


class MyObjectAPIView(UpdateModelMixin, RetrieveModelMixin, DestroyModelMixin, MyBaseAPIView):
    http_method_names = ['get', 'put', 'delete', 'options']
    # http methods

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, args, kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, args, kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, args, kwargs)

    # perform methods
    def perform_update(self, serializer):
        serializer.instance.modified_by = self.request.user
        serializer.save()

    def perform_destroy(self, instance):
        instance.enable = False
        instance.modified_by = self.request.user
        instance.save()
