from copy import deepcopy
from django.contrib import admin

# Register your models here.


class BaseAdmin(admin.ModelAdmin):
    list_display = ()

    def __init__(self, *args, **kwargs):
        base_fields = (
            'created_by',
            'created_at',
            'modified_at',
            'disabled_at'
        )
        if (not 'created_by' in self.list_display):
            self.list_display += base_fields
        if (not 'created_by' in self.list_filter):
            self.list_filter += base_fields

        return super(BaseAdmin, self).__init__(*args, **kwargs)
