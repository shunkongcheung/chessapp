
# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'vvra_9*n2)hujj-iywi)ei*mdb(hxga5sh8hb5n4(4g8ex+4-7'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'postgres',
        'USER': 'postgres',
        'HOST': 'localhost',
        'PORT': '5433',
    }
}

BROKER_URL = 'redis://localhost:6379'
CELERY_RESULT_BACKEND = 'redis://localhost:6379'
