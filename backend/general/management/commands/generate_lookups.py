from django.core.management.base import BaseCommand, CommandError
from general.models import LookUpTable


class Command(BaseCommand):
    help = 'General lookups pairs if not exists'

    def add_arguments(self, parser):
        pass

    def get_lookups(self):
        return [
            {
                'lookup_group': LookUpTable.GRP_CHESS[0],
                'lookup_key':'START_DEPTH',
                'lookup_value':5
            },
        ]

    def handle(self, *args, **options):
        lookups = self.get_lookups()
        for lookup in lookups:
            self.update_or_create_lookup(lookup)

    def update_or_create_lookup(self, lookup):
        LookUpTable.objects.update_or_create(
            lookup_group=lookup['lookup_group'],
            lookup_key=lookup['lookup_key'],
            defaults=lookup
        )
