from base.utils import (
    get_value_in_correct_type,
)
from django.db import models
from django.utils.translation import ugettext as _


# Create your models here.


class LookUpTable(models.Model):
    GRP_CHESS = ('GRP_CHESS', _("Chess"))
    lookup_group = models.CharField(
        choices=[GRP_CHESS],
        max_length=32,
    )
    lookup_key = models.CharField(max_length=32)
    lookup_value = models.TextField()

    @classmethod
    def get_lookup(cls, group, key):
        qs = LookUpTable.objects.filter(
            lookup_group=group,
            lookup_key=key,
        )
        if qs.count() != 1:
            raise Exception(
                f'Unable to filter lookups. ' +
                f'found {qs.count()} when filtering for {group} and {key}.'
            )
        value = qs.first().lookup_value

        return get_value_in_correct_type(value)
