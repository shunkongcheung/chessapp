from django.contrib import admin

from .models import (
    LookUpTable,
)

# Register your models here.


class LookUpTableAdmin(admin.ModelAdmin):
    list_display = (
        'lookup_group',
        'lookup_key',
        'lookup_value',
    )
    search_fields = (
        'lookup_group',
        'lookup_key',
    )
    list_filter = ('lookup_group',)


admin.site.register(LookUpTable, LookUpTableAdmin)
