# Generated by Django 2.2 on 2019-04-20 11:59

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='LookUpTable',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lookup_group', models.CharField(choices=[('GRP_CHESS', 'Chess')], max_length=32)),
                ('lookup_key', models.CharField(max_length=32)),
                ('lookup_value', models.TextField()),
            ],
        ),
    ]
