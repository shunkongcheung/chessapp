from base.models import BaseModel
from django.db import models
from django.utils.translation import ugettext as _


class SystemLog(BaseModel):
    LVL_DEBUG = "DEBUG"
    LVL_INFO = "INFO"
    LVL_ERROR = "ERROR"
    level = models.CharField(
        choices=[
            (LVL_DEBUG, _("Debug")),
            (LVL_INFO, _("Info")),
            (LVL_ERROR, _("Error")),
        ],
        max_length=16
    )
    position = models.CharField(max_length=128)
    message = models.TextField()
