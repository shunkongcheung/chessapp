from base.admin import BaseAdmin
from django.contrib import admin

from .models import (
    SystemLog,
)


class SystemLogAdmin(BaseAdmin):
    list_display = ['level', 'position', 'message', ]
    list_filter = ['level', ]
    search_fields = ['position', 'message', ]


admin.site.register(SystemLog, SystemLogAdmin)
